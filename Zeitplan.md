# Zeitplan:

## Do:

bis 18 Uhr	    Anreise

bis 19 Uhr	    Abendessen

ab 19:30 Uhr	Willkommen / Kennenlernspiel

ab 20 Uhr	    Einführungs-Vortrag

ab 20:30 Uhr	Bars in der Umgebung erkunden - Liste mit Fragen zum Kennenlernen | Film



## Fr:

bis 9 Uhr	    Frühstück / ggf. Anreise

ab 9:15 Uhr	    Vortrag von Externem (Thema: ???) 30 min + 15min Fragesession (Slido)

10 - 12 Uhr	    Workshop 1a | Workshop 1b | Workshop 1c | Workshop 1d

12 - 13 Uhr 	Mittagessen

13  - 14 Uhr	Schlafen / Meditieren / Spazieren

14 - 16 Uhr 	Workshop 2a | Workshop 2b | Workshop 2c | Workshop 2d

16 - 17 Uhr 	Kleingruppen-Besprechungen 

17 - 18 Uhr	    Opinion Speed Dating

18 - 19 Uhr 	Abendessen

ab 19 Uhr   	Lightning Talks max. 4min

ab 20 Uhr	    Externer Vortrag XY max 30min + 15min Fragen

ab 21 Uhr	    Bars & Open End




## Sa:

bis 9 Uhr	    Frühstück

ab 9:15 Uhr	    Vortrag von Externem (Thema: ???) 30 min + 15min Fragesession (Slido)

10 - 12 Uhr     Workshop 5 | Workshop 6

12 - 13 Uhr 	Mittagessen

13  - 14 Uhr	Schlafen / Meditieren / Spazieren

14 - 16 Uhr	    Workshop 7 | Workshop 8

16 - 17 Uhr 	Kleingruppen-Besprechungen 

17 - 18 Uhr	    Irgendein Spiel??

18 - 19 Uhr 	Abendessen

ab 19 Uhr 	    Externer Vortrag XY

ab 20 Uhr	    Fischbowl Diskussion




## So:

bis 9 Uhr	    Frühstück

ab 9:15         Uhr	Vortrag von Externem (Thema: ???) 30 min + 15min Fragesession (Slido)

10 - 12 Uhr	    Zusammenfassung in Dokumentform / How-To's und Austausch in Kleingruppen. 

12 - 12:30 Uhr	Abschlussrunde und Evaluation

12 - 13 Uhr 	Mittagessen

ab 13 Uhr 	    Abreise



## Vorträge:
- Die Macht von Defaults
- Wie viel weiß man über mich? Wo gebe ich überall informationen über mich preis?
- Kritik an Silicon Valley
- Wie Computerprogramme unsere menschlichen Schwächen ausnutzen (und was man dagegen tuen kann)
- Die Basics von Computer & Internet
- Interviews vor Publikum mit Dozenten
- Daten über die Thematik: Einfach einiges an interessanten Statistiken zusammentragen.
- Rechte und Pflichten im Internet - juristische Feinheiten

## Workshops:
- Hacking: Laptop, Smartphone, Websites, ...
- Das richtige Computer Setup Teil 1 (Passwörter, Passworttresor, Backups)
- Das richtige Computer Setup Teil 2 (Festplattenverschlüsselung, etc.)
- Anonym im Internet
- Wahrheit im Internet: wie verifiziere ich Informationen?
- Sichere Kommunikation mit der Außenwelt
- Meine Identität / Accounts im Internet aufräumen (und wie man zukünftig vorgehen sollte...)
- Cryptographie und Sicherheitstheorie
- Kryptowährungen?
- Advocatus Diabolus: Digitale Mündigkeit und Privatsphäre sind eine Utopie
- Sei der Herr über Dein Handy und nicht umgekehrt: Wie man sein Handy sinnvoll nutzt.

## Sonstiges:
- Hardware Showroom
- Betriebssystem Show
- Best of 'Ressourcen für digitale Mündigkeit'