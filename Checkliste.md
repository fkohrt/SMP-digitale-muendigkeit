# Zeitplan / Checkliste 

## 10 Monate vorher
- [x] Bildung eines Projektteams: **Max und Flo**
- [x] Wann soll die Tagung stattfinden? **27. bis 30. Juni 2019**
- [x] Idee zum Seminarinhalt und möglichen Dozent/innen entwickeln: [Projektbeschreibung](Projektbeschreibung.md)
- [x] Welche Thematik? Welche Zielgruppe/n? **Digitale Mündigkeit, Anfänger**
- [x] Ersten Ablaufplan skizzieren: **[Zeitplan](Zeitplan.md)**
- [ ] Terminkalender erstellen und wichtige Daten festlegen
- [x] Räumlichkeiten suchen: **[Ort](Ort.md)**
- [x] Erste Anfrage zu Termin und Thema in der Geschäftsstelle
## 9 Monate vorher
- [ ] Detailliertes Exposé schreiben
- [ ] Erste Kostenkalkulation
- [ ] Antragsstellung an die Studienstiftung  Achtung: Zeit zur Überarbeitung einplanen
- [ ] Nach Erhalt der Förderzusage: Buchung des Tagungsortes oder der Uni-Räume (hängt stark vom Termin ab) 
- [ ] Klären mit der Geschäftsstelle ob Ankündigung im Newsletter möglich ist
## 9 bis 6 Monate vorher	
- [ ] Einladung der Dozenten
## 5 bis 4 Monate vorher
- [ ] Klären: 
- [ ] Reinigung (vor, während und nach der Veranstaltung)
- [ ] Informationsmaterial, Arbeitsmaterialien
- [ ] Ablaufplanung der Veranstaltung:
- [ ] Zeitablauf
- [ ] Welche Aktivitäten finden parallel statt?
- [ ] Pausen
- [ ] Wo soll ggf. das Catering stattfinden?
- [ ] Raumverteilung klären
- [ ] Ggf. Abstellraum für Gepäck
## 4 bis 3 Monate vorher
- [ ] Abfrage Titel (inkl. Abstracts) der Vorträge an die Dozent/innen und technische Bedarf & Arbeitsmaterialien
- [ ] Verpflegung endgültig klären
- [ ] Tagungsgetränke
## 3 Monate vorher	
- [ ] Ankündigungstexte sollten da sein
- [ ] Endgültigen Tagungsplan festlegen
- [ ] Ausschreibungstext von der Geschäftsstelle genehmigen lassen

Technik klären:
- [ ] Internet-Zugang vorhanden? 
- [ ] Laptops
- [ ] Beamer
- [ ] Tafeln/Flip-Charts
- [ ] Wer ist für Fotos verantwortlich?
## 2 ½ Monate vorher	
- [ ] Offizielle Ausschreibung der Tagung im Intranet, dabei ggf. abfragen: Alternativwahl zur Arbeitsgruppe, Vegetarier, Einverständnis zur Veröffentlichung von Fotos
- [ ] Anmeldungen gehen direkt an das Planungsteam; Anmeldefrist von mindestens einem Monat 
- [ ] Information an die Dozenten über Zeitplan der Vorbereitungen schicken
- [ ] Vorschuss in der Geschäftsstelle beantragen
- [ ] Materialien in der Geschäftsstelle anfragen
## 1 ½ Monate vorher	
- [ ] Zu- (und ggf. Absagen) mit weiteren Informationen versenden (Vergabe der Themen, inhaltliche Vorbereitung, Eigenbeteiligung, Anreise, Freizeitmöglichkeiten vor Ort) 
- [ ] Teilnehmerliste als word.doc an Geschäftsstelle
- [ ] Informationen (Teilnehmerliste, Verlaufsplan, Technikwünsche) an das Tagungshaus schicken, ggf. Plätze stornieren 
- [ ] Ggf. den Dozenten die Liste ihrer Arbeitsgruppe schicken; diese vergeben (Referats-) Themen an die Teilnehmer
## 2 Wochen vorher
- [ ] Erinnerungsemail an Studenten und Dozenten versenden mit allen wichtigen Informationen 
- [ ] Besprechung mit allen Helfern
- [ ] Geräte checken
## 1 Woche vorher
- [ ] Teilnehmerunterlagen
- [ ] Referentenunterlagen
- [ ] Tagungs-Programm
ggf. Fahrpläne, Stadtpläne
- [ ] Teilnehmerliste vorbereiten zum Unterschreiben
- [ ] Reisekostenformulare der Geschäftsstelle für die Dozent/innen
- [ ] Evaluationsbögen
- [ ] Schlüssel für Tagungsräume
- [ ] Wegeleitung/Beschilderung
## Während der Veranstaltung
- [ ] Einsammeln Eigenbetrag und Unterschriften der Teilnehmer; fehlende Teilnehmer vermerken
- [ ] Ausgabe Evaluationsbogen an die Teilnehmer 
- [ ] Danksagungen 
- [ ] Zeitplan im Blick behalten
- [ ] Die Veranstaltung genießen!
## Nach der Veranstaltung
- [ ] Im Organisationsteam gemeinsam die Ergebnisse der Veranstaltung besprechen (Was war besonders gut? Wo hat es evt. gehakt?)
- [ ] Endabrechnung fertigstellen 
- [ ] Evaluationen auswerten
- [ ] Unterlagen an die Geschäftsstelle
- [ ] Abschließendes Telefonat mit der Ansprechpartnerin in der Geschäftsstelle (Feedback, Auswertung, Hinweise zu Stipendiaten machen Programm)
- [ ] Bericht schreiben
- [ ] Fotos auswählen
- [ ] Dankeschön-Email an die Dozent/-innen


