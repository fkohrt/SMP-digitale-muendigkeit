# Organisatorische Eckpfeiler

## formale Ziele + Maßnahmen
- Breitenwirkung _möglichst viele, möglichst verschiedene Menschen sollen mit der Veranstaltung erreicht werden_
  - obere Grenzen für Teilnehmerzahl ausnutzen: 100
  - Ausschreibung zusätzlich in MWP (geringer Aufwand), ggf. in anderen Förderwerken (Aufwand unbekannt)
  - Vorträge auch für Allgemeinheit öffnen (dafür wäre Kinosaal gut)
  - Barrierefreiheit der Unterkunft sowie der digitalen Materialien
  - Veröffentlichung aller Aufzeichnungen und Materialien _auch Menschen, die nicht dabei waren, gehören zur Zielgruppe_
    - Video-Aufzeichnung der Vorträge
    - Vortragsfolien (mit Notizen, sofern vorhanden)
    - Protokollierung der Workshop-Inhalte
- Effektivität _langfristiger und nachhaltiger Einfluss der Veranstaltung auf Denken und Handeln_
  - Vorbereitung durch Lesen oder Schreiben (Essay), ggf. Vorab-Downloads
  - Nachbereitung durch Veröffentlichung der Materialien
  - Veränderungen der Technik-Nutzung **während der Veranstaltung** möglich machen und unterstützen
  - Möglichkeiten der Weiterbildung nach der Veranstaltung aufzeigen
    - Literaturliste
    - Websites (Support, Know-How, News)
    - lokale Strukturen: repair cafe, hacker space, Vereine (CCC, etc.), Informatik-Fachschaft
    - zukünftige Veranstaltungen (re:publica, FIfFKon, etc.)
  - Kompetenzen zur Selbst-Hilfe fördern
  - Ängste abbauen (z. B. Linux kompliziert) durch Herstellen eines Erstkontakts
  - Vorteile aufzeigen (kostenlos, ästhetisch, zeitsparend, sicher), auch durch Vorleben während Planung und Durchführung
  - Ideale verbreiten (etwa freies Wissen, autonomer Nutzer)
- Multiplikatorfähigkeit _eine Vorlage für ähnliche Veranstaltungen sein_
  - Dokumentation der Planung und Durchführung (öffentliches Git-Verzeichnis), auch Notizen und Gedanken veröffentlichen
  - Reflektion am Ende, ggf. mit Verbesserungsvorschlägen

## inhaltliche Ziele + Formate
- Relevanz des Themas vermitteln (Vortrag)
  - Beispiele: Unsicherheit eines Windows-Passworts, E-Mail-Absender nicht vertrauenswürdig, politische Macht von GAFAM, gesellschaftlicher Einfluss von GAFAM
  - Funktionen: Interesse wecken, persönliche Betroffenheit aufzeigen, unterhalten
  - Bemerkung: Beispiele müssen _stark_ sein und den Zuschauer am besten einbinden! Also nicht _einen_ PC knacken, sondern _den des Zuschauers_. Evtl. live hack oder hack über die Tage hinweg?
- Ideale (open knowledge, free/libre software) und Akteure (Digitalcourage, Netzpolitik, Forum Privatheit, etc.) vermitteln bzw. vorstellen (Vortrag? Oder nebenbei...)
- best practices vorstellen _praktische, direkt umsetzbare Tools und Maßnahmen vorstellen und ein- bzw. umsetzen_ (Workshops)
- tiefer gehen: analysieren, diskutieren, reflektieren (Vortrag, Workshop)

## Randbedingungen
- Umweltschutz
  - möglichst wenig ausdrucken
  - keine Verwendung von Einmal-Artikeln
  - zur Nutzung öffentlicher Verkehrsmittel anregen
- Datensparsamkeit
  - Foto-Widerspruch anbieten
- Datenschutz
  - Teilnehmerdaten verschlüsselt aufbewahren
  - Maßnahmen zur Sicherung dieser Computer ergreifen
  - Daten löschen, sobald nicht mehr benötigt
- _free culture_
  - konsequente Nutzung freier Software
