# Treffen 10.11.2018

## Zeitplan
ab Mitte April: Vorbereitung für Max Prüfungen Mitte Mai
Juni / Juli 2019

Anreise Donnerstagabend

## Was wollen wir?
- Praxis-Teil
- nebenher Theorie
- Hacking-Session: Möglichkeiten aufzeigen, Bewusstsein schaffen
- coole Speaker

## Wo
Berlin

## nächstes ToDo
### Jetzt gleich
- Kontakt mit Stiftung aufnehmen: Flo
- Antrag vorbereiten: Max

### Weiter in der Zukunft
- Unterkunft klären
- Speaker einladen